# gitlab-runner-nvme

Run Gitlab CI jobs in an ephemeral virtual machine with an emulated NVMe device.
Each virtual machine is spun up fresh from the base image for every job. This
allows running otherwise impure and possibly destructive tests against a unique
device.

This has some limitations. Refer to [Qemu's documentation][qemu-doc] for more
information.

## How it works.

A base image is generated to include the necessary packages for CI. For every
job, a fresh qcow2 overlay is made on top of the base image, and another image
is made as the backing for the NVMe device. This overlay is then booted with the
NVMe device attached. Once the machine's network and ssh daemon are up, the job
script is executed using ssh within the machine.

## How it's made.

Deploying this project is relatively simple compared to the manual alternative.
Deployment is performed using [terraform][terraform] and [packer][packer].
Packer produces the base image and nests it within another image for the actual
Gitlab runner host. Terraform is then used to deploy this image to
[DigitalOcean][digitalocean].

The runner's image is intentionally designed based off an image that can be used
elsewhere whether it be metal or another cloud provider. It is based off the
[generic debain 11 image][debian-cloud] which is bootable on OpenStack,
DigitalOcean, and any other cloud-init aware cloud.

> ***NOTE***: Building the image requires an unreleased feature of packer
(`cd_content`). This should be available in the next release, but for right now
you must install the dev version of [packer-plugin-qemu][packerqemu].

Build Gitlab Runner image (gitlab-runner-nvme.img.gz):

```bash
packer init runner.pkr.hcl
packer build -parallel-builds=1 runner.pkr.hcl
```

Deploy to DigitalOcean:

```bash
terraform init -backend local
terraform apply
```

[qemu-doc]: https://qemu.readthedocs.io/en/latest/system/devices/nvme.html#controller-emulation
[terraform]: https://www.terraform.io/
[packer]: https://www.packer.io/
[digitalocean]: https://www.digitalocean.com/
[debian-cloud]: https://cloud.debian.org/images/cloud/
[packerqemu]: https://github.com/hashicorp/packer-plugin-qemu
