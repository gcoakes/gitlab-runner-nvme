# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.11.1"
  constraints = "~> 2.0"
  hashes = [
    "h1:6PNfB68DDo5RkXhepEUSt95bZgT4i8J2eJNcaQTxlCs=",
    "zh:0b379be98de2927eb751db4e88e69b14c979b97f33ff5b3f7840e27bbb3b10c4",
    "zh:28cdc581a684681eee6c871da76f0a6e9a99c4ae7dfabb0ffbc62c2566299a3b",
    "zh:41c332b015af4b0b08c7f34c57bd1f6e687ab78134d8fa891aba2357e2b0ced5",
    "zh:537da9f53842fe9c3ee3d8ae7ff8ee5a847a879ec881a9cf087e677d36e642c0",
    "zh:621782735bb0fa0e9b12750a6e91c2ef0d144185201435f0f70a95b66aa08dd1",
    "zh:7f59913181a0fb6d78ccfdac19f17d4b693f67776baf8ffb6a9cd07088804be8",
    "zh:9b164e204085691876535233eb57cd6fdf223a0eff529ceb072b61f4fea2f950",
    "zh:9d3c87e40b11f19bb0e59b369663d05b03e37d3095200a7b40e7bc7e04e3a303",
    "zh:9fe3ec149fcbe25f987b02e59d810a8fd836a9f4b4b7a9ed7273800c8020cc87",
    "zh:a426e765a4df74b21196d2faf4786bec2a7fdc2c21bf1137a7fb9a1c7e1cf1fc",
    "zh:aae6361881b90802d5fad20ca2cd7330e118be09cf583b79b9276e033b09beb4",
    "zh:c760535090ec85194e8248c823439a1692be72e4f337916f63c742ee8cb1f7cd",
    "zh:eca7a79f7c3611b79a1013b3390ac917043ac19cd648bda8c87e4b7714b27b00",
    "zh:f221a20df002cffac09dba1734a1f77217dd546705fd1b123951d03c0fff6592",
    "zh:f62b7120420cc0d3ff9d291aa1aae20b5868dd47d453cb3a33b915615f5cdb29",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "1.20.2"
  constraints = "1.20.2"
  hashes = [
    "h1:vr6CnniYpgupv3zAcDYPYvqadhqwreWFRc88y3FWNhA=",
    "zh:1236802b9257de66ad3580eb69605ae08511da70112741587e8d426ed13f96c9",
    "zh:1604df9ef1a3546e696463d626fe18dc8fa7e6a75e973e5a268125a32251c73d",
    "zh:20c994a1156d7b0a688efb117f5314ae56a1aba303bde8aee44bbceb184801c1",
    "zh:30b455dfb076e29e50a173d72bec19ecc7f33281d8030b5e973d09ceada2df43",
    "zh:3592def3e1ab34d0a52058ca46ce07f2cee5f7dc6694585f3201d596c71e9bca",
    "zh:45b05aaef909d4a065a5b53dd347217e27880566f5e0c9142047d20355990554",
    "zh:495e02530a66473134514e471ee0e9a3914646235cb08fe8f0ddeeda339bf5b2",
    "zh:49ee058e35c8892e69d79ce4ddc43992454e04a5c69dbafcfea9b26804df9630",
    "zh:5a8b1b94d5fd50bb69a7b88cb45c26434109b671ab9b77a9851973411d2a7baf",
    "zh:69f7e2dd14ce80fd87810d630d799e7f116c93ec9b3c11fdd9e4f2c8054dd21f",
    "zh:deacfbbcb763a5fb42dd547b68ee10e5279ec5617f949b9b7fa800fc5ed0ded0",
    "zh:ded238d3f0972fe1338ee9e2a02a7652b5fafb237b3d9d7c2f8b53f94de8c4bb",
    "zh:f70bc23f4bf4e4365bd1712cd4dafaa7960643519deae9dafcea473f77b9b26f",
  ]
}
