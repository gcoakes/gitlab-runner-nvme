packer {
  required_plugins {
    sshkey = {
      version = ">= 0.1.0"
      source  = "github.com/ivoronin/sshkey"
    }
  }
}

data "sshkey" "install" {}

data "sshkey" "host_to_guest" {}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

source "qemu" "debian11" {
  iso_url              = "https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2"
  iso_checksum         = "file:https://cloud.debian.org/images/cloud/bullseye/latest/SHA512SUMS"
  headless             = true
  cpus                 = 1
  memory               = 2048
  disk_image           = true
  accelerator          = "kvm"
  ssh_username         = "root"
  ssh_private_key_file = data.sshkey.install.private_key_path
  cd_label             = "cidata"
  cd_content = {
    "meta-data" = jsonencode({
      instance-id    = "iid-{{ .Name }}-${local.timestamp}"
      local-hostname = "{{ .Name }}-${local.timestamp}"
    })
    "user-data" = <<-EOF
      #cloud-config
      package_upgrade: yes
      users:
      - name: root
        ssh_authorized_keys:
        - ${data.sshkey.install.public_key}
        sudo: ALL=(ALL) NOPASSWD:ALL
      EOF
  }
  shutdown_command = "cloud-init clean; echo 'packer' | sudo -S shutdown -P now"
}

build {
  source "qemu.debian11" {
    vm_name          = "guest-base"
    name             = "guest-base"
    output_directory = "output-guest-base"
    format           = "qcow2"
  }
  provisioner "shell" {
    inline = [
      "mkdir -p /root/.ssh",
      "echo '${data.sshkey.host_to_guest.public_key}' >> /root/.ssh/authorized_keys",
    ]
  }
  provisioner "shell" {
    script = "provision/guest.sh"
  }
}

build {
  source "qemu.debian11" {
    vm_name          = "gitlab-runner-nvme"
    name             = "gitlab-runner-nvme"
    output_directory = "output-gitlab-runner-nvme"
    format           = "raw"
  }
  provisioner "shell" {
    inline = [
      "mkdir -p /usr/local/libexec/gitlab-runner-nvme /root/.ssh /var/lib/libvirt/images",
      "echo '${data.sshkey.host_to_guest.public_key}' >> /root/.ssh/id_rsa.pub",
    ]
  }
  provisioner "file" {
    source      = data.sshkey.host_to_guest.private_key_path
    destination = "/root/.ssh/id_rsa"
  }
  provisioner "file" {
    source      = "output-guest-base/guest-base"
    destination = "/var/lib/libvirt/images/guest-base.qcow2"
    generated   = true
  }
  provisioner "file" {
    sources = [
      "libexec/base.sh",
      "libexec/cleanup.sh",
      "libexec/prepare.sh",
      "libexec/run.sh",
    ]
    destination = "/usr/local/libexec/gitlab-runner-nvme/"
  }
  provisioner "file" {
    source      = "register.sh"
    destination = "/usr/local/bin/gitlab-runner-register"
  }
  provisioner "shell" {
    script = "provision/runner.sh"
  }
  post-processors {
    post-processor "compress" {
      output = "gitlab-runner-nvme.img.gz"
    }
  }
}