#!/usr/bin/env bash
scriptdir="$(dirname -- "$(realpath "$0")")"
# shellcheck source=./base.sh
. "$scriptdir/base.sh"

VM_IP=$(_get_vm_ip)

if ! ssh -i /root/.ssh/id_rsa -o StrictHostKeyChecking=no root@"$VM_IP" /bin/bash < "${1}"; then
    # Exit using the variable, to make the build as failure in GitLab
    # CI.
    exit "$BUILD_FAILURE_EXIT_CODE"
fi