#!/usr/bin/env bash
# shellcheck source=./base.sh
. "$(dirname -- "$(realpath "$0")")/base.sh"

set -eo pipefail

# trap any error, and mark it as a system failure.
# shellcheck disable=SC2064
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

qemu-img create -f qcow2 -F qcow2 -b "$BASE_VM_IMAGE" "$VM_IMAGE"
qemu-img create -f raw "$NVME_IMAGE" "10M"
chown "libvirt-qemu:libvirt-qemu" "$VM_IMAGE"
chown "libvirt-qemu:libvirt-qemu" "$NVME_IMAGE"

virt-install \
    --name "$VM_ID" \
    --os-variant ubuntu20.04 \
    --disk "$VM_IMAGE" \
    --import \
    --vcpus=1 \
    --ram=2048 \
    --network default \
    --graphics none \
    --noautoconsole \
    --qemu-commandline="-drive format=raw,file=$NVME_IMAGE,if=none,id=nvm -device nvme,serial=QEMUTESTIMGSERIAL123,drive=nvm"

# Wait for VM to get IP
echo 'Waiting for VM to get IP'
for i in $(seq 1 30); do
    VM_IP=$(_get_vm_ip)

    if [ -n "$VM_IP" ]; then
        echo "VM got IP: $VM_IP"
        break
    fi

    if [ "$i" == "30" ]; then
        echo 'Waited 30 seconds for VM to start, exiting...'
        # Inform GitLab Runner that this is a system failure, so it
        # should be retried.
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1s
done

# Wait for ssh to become available
echo "Waiting for sshd to be available"
for i in $(seq 1 60); do
    if ssh -i /root/.ssh/id_rsa -o StrictHostKeyChecking=no root@"$VM_IP" >/dev/null 2>/dev/null; then
        break
    fi

    if [ "$i" == "30" ]; then
        echo 'Waited 30 seconds for sshd to start, exiting...'
        # Inform GitLab Runner that this is a system failure, so it
        # should be retried.
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1s
done

ssh-keygen -R "$VM_IP"