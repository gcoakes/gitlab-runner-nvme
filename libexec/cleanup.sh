#!/usr/bin/env bash
# shellcheck source=./base.sh
. "$(dirname -- "$(realpath "$0")")/base.sh"

set -eo pipefail

# Destroy VM.
virsh shutdown "$VM_ID"

# Undefine VM.
virsh undefine "$VM_ID"

# Delete VM disk.
if [ -f "$VM_IMAGE" ]; then
    rm "$VM_IMAGE"
fi

if [ -f "$NVME_IMAGE" ]; then
    rm "$NVME_IMAGE"
fi