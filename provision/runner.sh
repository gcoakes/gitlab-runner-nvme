#!/bin/sh
wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner

# Wait for cloud-init to complete.
while ! grep "Cloud-init .* finished" /var/log/cloud-init.log; do
    echo "$(date -Ins) Waiting for cloud-init to finish"
    sleep 2
done

mkdir -p /var/lib/gitlab-runner
gitlab-runner install --user=root --working-directory=/var/lib/gitlab-runner
gitlab-runner start

export DEBIAN_FRONTENT=noninteractive
apt-get install -y libvirt-daemon-system virtinst
# Enable the default network so the guests can establish a network.
virsh net-start default
# Fix permissions in the images path, so virt-install can launch.
chown -R libvirt-qemu:libvirt-qemu /var/lib/libvirt/images
echo '/var/lib/libvirt/images/* rwk,' \
    >> /etc/apparmor.d/local/abstractions/libvirt-qemu