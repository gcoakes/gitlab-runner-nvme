#!/bin/sh

wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner

# Wait for cloud-init to complete.
while ! grep "Cloud-init .* finished" /var/log/cloud-init.log; do
    echo "$(date -Ins) Waiting for cloud-init to finish"
    sleep 2
done

export DEBIAN_FRONTENT=noninteractive
apt-get install -y python3 python3-pip python3-venv git-lfs git
ln -s /usr/bin/python3 /usr/bin/python

git lfs install --skip-repo
ssh-keyscan gitlab.com >> ~/.ssh/knwon_hosts

sed 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"/' \
    -i /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg