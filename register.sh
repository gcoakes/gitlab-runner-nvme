#!/bin/sh
libexec=/usr/local/libexec/gitlab-runner-nvme
exec gitlab-runner register \
    --executor custom \
    --custom-prepare-exec="$libexec/prepare.sh" \
    --custom-run-exec="$libexec/run.sh" \
    --custom-cleanup-exec="$libexec/cleanup.sh" \
    --builds-dir=/var/lib/gitlab-runner/builds \
    --cache-dir=/var/lib/gitlab-runner/cache \
    "$@"