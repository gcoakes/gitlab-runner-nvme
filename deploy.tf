terraform {
  backend "http" {

  }
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "token" {
  type      = string
  sensitive = true
}
variable "spaces_access_id" {
  type      = string
  sensitive = true
}
variable "spaces_secret_key" {
  type      = string
  sensitive = true
}
variable "pubkey" {
  type = string
}
variable "runner_token" {
  type      = string
  sensitive = true
}
variable "region" {
  type    = string
  default = "nyc3"
}
variable "droplet_size" {
  type    = string
  default = "s-2vcpu-4gb"
}
variable "runner_domain" {
  type    = string
  default = "https://gitlab.com/"
}

provider "digitalocean" {
  token             = var.token
  spaces_access_id  = var.spaces_access_id
  spaces_secret_key = var.spaces_secret_key
}

resource "digitalocean_spaces_bucket" "images" {
  name   = "gitlab-runner-nvme-images"
  region = var.region
  acl    = "public-read"
}

resource "digitalocean_spaces_bucket_object" "image" {
  bucket = digitalocean_spaces_bucket.images.name
  region = digitalocean_spaces_bucket.images.region
  key    = "gitlab-runner-nvme.img.gz"
  source = "gitlab-runner-nvme.img.gz"
  acl    = "public-read"
}

resource "digitalocean_custom_image" "gitlab_runner_nvme_image" {
  name    = "gitlab-runner-nvme-image"
  url     = "https://${digitalocean_spaces_bucket.images.bucket_domain_name}/${digitalocean_spaces_bucket_object.image.key}"
  regions = [var.region]
}

resource "digitalocean_droplet" "gitlab_runner_nvme" {
  name     = "gitlab-runner-nvme"
  image    = digitalocean_custom_image.gitlab_runner_nvme_image.image_id
  region   = var.region
  size     = var.droplet_size
  ssh_keys = [var.pubkey]
  connection {
    type  = "ssh"
    user  = "root"
    host  = self.ipv4_address_private
    agent = true
  }
  provisioner "remote-exec" {
    inline = [
      "gitlab-runner-register -n -u ${var.runner_domain} -r ${var.runner_token} --tag-list nvme"
    ]
  }
}